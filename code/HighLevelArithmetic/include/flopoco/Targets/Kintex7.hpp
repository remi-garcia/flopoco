#ifndef Kintex7_HPP
#define Kintex7_HPP
#include "flopoco/Target.hpp"
#include <iostream>
#include <sstream>
#include <vector>


namespace flopoco{

	/** Class for representing an Kintex7 target */
	class Kintex7 : public Target
	{
	public:
		/** The default constructor. */  
		Kintex7();
		/** The destructor */
		~Kintex7();
		/** overloading the virtual functions of Target
		 * @see the target class for more details 
		 */

		// Overloading virtual methods of Target 
		double logicDelay(int inputs);

		double adderDelay(int size, bool addRoutingDelay=true);

		double eqComparatorDelay(int size);
		double ltComparatorDelay(int size);
		double eqConstComparatorDelay(int size);
		
		double lutDelay();
		double addRoutingDelay(double d);
		double fanoutDelay(int fanout = 1);
		double ffDelay();
		long   sizeOfMemoryBlock();
		double lutConsumption(int lutInputSize);
		double tableDelay(int wIn, int wOut, bool logicTable);

		// The following is a list of methods that are not totally useful: TODO in Target.hpp
		double adder3Delay(int size); // currently irrelevant for Xilinx
		double carryPropagateDelay();
		double DSPMultiplierDelay();
		double DSPAdderDelay();
		double DSPCascadingWireDelay();
		double DSPToLogicWireDelay();
		
		double LogicToDSPWireDelay(){ return 0;} //TODO

		double RAMDelay() { return RAMDelay_; }
		double LogicToRAMWireDelay() { return RAMToLogicWireDelay_; }

		/** 
		 * The four 6-LUTs of a slice can be combined without routing into a 
		 * 8-LUT
		 */
		int maxLutInputs() { return 8; }
		
	private:

		// The following is copypasted from Vivado timing reports
		const double lut5Delay_ = 0.043e-9;       /**< The delay of a LUT, without any routing (cut from vivado timing report)*/
		const double lut6Delay_ = 0.119e-9;       /**< The delay of a LUT, without any routing (cut from vivado timing report)*/
		const double ffDelay_ = 0.216e-9;       /**< The delay of a flip-flop, without any routing  (cut from vivado timing report)*/
		const double carry4Delay_ = 0.049e-9;    /**< The delay in the middle of the fast carry chain   */
		const double adderConstantDelay_  =  0.124e-9 + 0.260e-9 + 0.159e-9; /**< includes a LUT delay and the initial and final carry4delays*/
		const double fanoutConstant_ = 1e-9/65 ; /**< Somewhere in Vivado report, someday, there has appeared a delay of 1.5e-9 for fo=65 */
		const double typicalLocalRoutingDelay_ = 0.5e-9;
		const double DSPMultiplierDelay_ = 2.392e-9  + 0.5e-9; // Vivado reports 2.392 for a logic DSP48 but we add te routing delay
		const double RAMDelay_ = 2e-9; // TODO
		const double RAMToLogicWireDelay_= 0; // TODO
	};

}
#endif
