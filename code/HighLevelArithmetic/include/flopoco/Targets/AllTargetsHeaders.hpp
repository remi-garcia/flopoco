#ifndef ALLTARGETSHEADERS_HPP
#define ALLTARGETSHEADERS_HPP

/* targets ---------------------------------------------------- */
#include "flopoco/Target.hpp"
// #include "flopoco/TargetModel.hpp"

//#include "flopoco/Targets/Spartan3.hpp"
//#include "flopoco/Targets/Virtex4.hpp"
//#include "flopoco/Targets/Virtex5.hpp"
#include "flopoco/Targets/Virtex6.hpp"

//#include "flopoco/Targets/StratixII.hpp"
//#include "flopoco/Targets/StratixIII.hpp"
//#include "flopoco/Targets/StratixIV.hpp"
#include "flopoco/Targets/StratixV.hpp"

//#include "flopoco/Targets/CycloneII.hpp"
//#include "flopoco/Targets/CycloneIII.hpp"
//#include "flopoco/Targets/CycloneIV.hpp"
//#include "flopoco/Targets/CycloneV.hpp"
#include "flopoco/Targets/Zynq7000.hpp"
#include "flopoco/Targets/Kintex7.hpp"
#include "flopoco/Targets/VirtexUltrascalePlus.hpp"

#include "flopoco/Targets/Versal.hpp"

#include "flopoco/Targets/ManualPipeline.hpp"
#endif //ALLTARGETSHEADERS_HPP
